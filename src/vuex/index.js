import vue from 'vue'
import vuex from 'vuex'

import actions from './actions'
import mutations from './mutations'

vue.use(vuex)

const state = {
    profile: {},
    canteenInfo: null,
    name:"zengjielin",
    isWeixinJSBridgeReady: false,
}

export default new vuex.Store({
    state,
    actions,
    mutations,
})