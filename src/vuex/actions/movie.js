import axios from 'axios'

export default {
    getHotMovieList({ commit }, params) {
        return axios.get('/v2/movie/in_theaters', params)
    },
}